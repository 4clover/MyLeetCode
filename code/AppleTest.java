package code;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class AppleTest {

	/*
	 *ƻ���� 
	 */
	class Apple{
		public Apple(){
			
		}
	}
	/*
	 * ������
	 */
	class Box{
		static final int MaxValue=5;
		Apple[] apples=new Apple[MaxValue];
		
		int putIndex=0;
		int takeIndex=0;
		int count=0;
		/*
		 * �����-�����ģʽ������������
		 */
		final Lock lock=new ReentrantLock();
		final Condition notFull=lock.newCondition();
		final Condition notEmpty=lock.newCondition();
		/*
		 * ȡƻ���������������е�ƻ�����������������5��
		 * ��ô����ǰ�̣߳�ֱ�������е�ƻ������5����Ȼ���ٷ���������
		 * ���ּ���Ĳ��Ժܿ��ܲ�������
		 */
		
		public void putApple(Apple apple) throws InterruptedException{
			lock.lock();
			try{
				while(count==MaxValue)
					notFull.await();
				apples[putIndex++]=apple;
				if(putIndex==MaxValue) 
					putIndex=0;
				count++;
				notEmpty.signal();
			}finally{
				lock.unlock();
			}
		}
		/*
		 * ȡƻ���������������û��ƻ�����õ�ǰ�̵߳ȴ�ֱ����������ƻ��Ϊֹ
		 */
		public Apple takeApple() throws InterruptedException{
			Apple apple=null;
			lock.lock();
			try{
				while(count==0)
					notEmpty.await();
				apple=apples[takeIndex++];
				count--;
				if(takeIndex==MaxValue)
					takeIndex=0;
				notFull.signal();
				return apple;
			}finally{
				lock.unlock();
			}
		}
	}
}
