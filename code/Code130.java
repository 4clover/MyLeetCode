package code;

import java.util.Stack;

public class Code130 {
	
	public static class Point{
		int x,y;
		Point(int a,int b){
			x=a;
			y=b;
		}
	}
	int n,m;
	boolean[][] vis;
	int[][] direction={{0,1},{0,-1},{1,0},{-1,0}}; 
	
	public void search(int x,int y,char[][] board,char c){
		Stack<Point> stack=new Stack<Point>();
		//开始查找所有的o能到的o
		Point start=new Point(x, y);
		stack.push(start);
		while(!stack.isEmpty()){
			start=stack.pop();
			if(vis[start.x][start.y]==true)
				continue;
			vis[start.x][start.y]=true;
			for(int j=0;j<4;j++){
				int s=start.x+direction[j][0];
				int t=start.y+direction[j][1];
				if(s>=0 && s<n && t>=0 && t<m && !vis[s][t] && board[s][t]==c){
					stack.push(new Point(s,t));
				}
			}
		}
	}
	
	public  void solve(char[][] board){
		
		int i,j;
		int n=board.length;
		if(n==0)	return ;
		int m=board[0].length;
		//标记所有不能被修改的点o
		vis=new boolean[n][m];
		//第0列和n-1列
		for(i=0;i<n;i++){
			if(board[i][0]=='O'){
				search(i,0,board,'O');
			}
			if(board[i][m-1]=='O'){
				search(i,m-1,board,'O');
			}
		}
		//第0行和第m-1行
		for(i=0;i<m;i++){
			if(board[0][i]=='O'){
				search(0,i,board,'O');
			}
			if(board[n-1][i]=='O'){
				search(n-1,i,board,'O');
			}
		}
		for(i=0;i<n;i++){
			for(j=0;j<m;j++){
				if(board[i][j]=='O' && !vis[i][j]){
					board[i][j]='X';
					vis[i][j]=true;
                }
				System.out.print(board[i][j]);
			}
			System.out.println();
		}

	}
	public static void main(String[] args){
		char[][] board={{'X','X','X'},{'X','X','X'},{'X','X','X'}};
		
		Code130 code=new Code130();
		code.solve(board);
	}
}
