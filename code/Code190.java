package code;

import java.util.Stack;

public class Code190 {
	
    public int reverseBits(int n) {
        Stack<Integer> stack=new Stack<Integer>();
        int x=0;
        if(n==0)
        	return 0;
        
        while(n!=0){
        	stack.push(n&1);
        	n>>=1;
        }
        int flag=stack.elementAt(0);
        int bit=30;
        for(int i=1;i<stack.size();i++){
        	x+=(stack.elementAt(i)<<bit);
        	bit--;
        }
        if(flag==0)
        	flag=-1;
        else flag=1;
        return x;
    }
    public static void main(String[] args){
    }
}
