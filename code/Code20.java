package code;

import java.util.Stack;

public class Code20 {
    public boolean isValid(String s) {
    	int i,j;
    	Stack<Character> sck=new Stack<Character>();
    	i=0;
    	while(i<s.length()){
    		if(s.charAt(i)=='(' || s.charAt(i)=='[' || s.charAt(i)=='{'){
    			sck.push(s.charAt(i));
    		}
    		else{
    			if(sck.isEmpty())
    				return false;
    			if(s.charAt(i)==')'){
    				if(sck.pop()!='(')
    					return false;
    			}
    			else if(s.charAt(i)==']'){
    				if(sck.pop()!='[')
    					return false;
    			}
    			else{
    				if(sck.pop()!='{')
    					return false;
    			}
    		}
    		i++;
    	}
    	if(sck.isEmpty() && i==s.length())
    		return true;
        return false;
    }
    public static void main(String[] args){
    	System.out.println(new Code20().isValid("()"));
    }
}
