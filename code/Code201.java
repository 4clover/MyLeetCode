package code;

import java.util.Stack;

public class Code201 {
	
	/*
	 * 求[n,m]区间内所有的元素的&的值
	 * 思路是：
	 * 因为2进制的思想是，地位增加法
	 * 如果将低位的不同元素的和谐掉，那么剩下部分不就是那个&不变的部分吗？
	 * 将n,m同时右移，让他们之间变化的数值&起来之后，肯定变成0，剩余的部分才是不变的
	 */
    public int rangeBitwiseAnd(int m, int n) {
        int bitCnt=0;
        int x=m,y=n;
        int ans=0;
        while(n!=m){
        	n>>=1;
        	m>>=1;
        	bitCnt++;
        }
        return (n<<bitCnt);
    }
    public static void main(String[] args){
    	System.out.println(new Code201().rangeBitwiseAnd(5, 6));
    }
}
