package code;

import java.util.HashSet;
import java.util.Set;

public class Code202 {
	
    public boolean isHappy(int n) {
    	Set<Integer> vis=new HashSet<Integer>();
    	vis.add(n);
    	
    	while(n!=1){
    		int sum=0;
    		while(n!=0){
    			sum+=(n%10)*(n%10);
    			n/=10;
    		}
    		if(sum==1)
    			return true;
    		n=sum;
    		if(vis.contains(n))
    			return false;
    		vis.add(n);
    	}
        return true;
    }
    public static void main(String[] args){
    	System.out.println(new Code202().isHappy(19));
    }
}
