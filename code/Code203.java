package code;

public class Code203 {
	
    public ListNode removeElements(ListNode head, int val) {
    	ListNode p,q;
    	while(head!=null && head.val==val)
    		head=head.next;
    	
    	if(head==null)
    		return head;
    	q=head;
    	p=head.next;
    	while(p!=null){
    		while(p!=null && p.val==val)
    			p=p.next;
    		q.next=p;
    		q=p;
    		if(p==null)
    			break;
    		p=p.next;
    	}
        return head;
    }
    public static void main(String[] args){
    	ListNode head=new ListNode(1);
    	ListNode node1=new ListNode(2);
    	ListNode node2=new ListNode(2);
    	head.next=node1;
    	node1.next=node2;
    	
    	head=new Code203().removeElements(head, 2);
    	while(head!=null){
    		System.out.println(head.val);
    		head=head.next;
    	}
    }
}
