package code;

import java.util.HashMap;
import java.util.Map;

public class Code206 {
    public ListNode reverseList(ListNode head) {
    	ListNode p,q = null,next = null;
    	if(head==null || head.next==null)
    		return head;
    	q=head;
    	p=head.next;
    	while(p!=null){
    		if(p!=null)
    			next=p.next;
    		p.next=q;
    		if(q==head){
    			q.next=null;
    		}
    		q=p;
    		p=next;
    	}
    	head=q;
        return head;
    }
    /*
     * 205
     */
    public boolean isIsomorphic(String s, String t) {
        Map<Character,Character> map=new HashMap<Character,Character>();
        for(int i=0;i<s.length();i++){
        	if(map.containsKey(s.charAt(i))){
        		char c=map.get(s.charAt(i));
        		if(c!=t.charAt(i))
        			return false;
        	}
        	else{
        		if(map.containsValue(t.charAt(i)))
        			return false;
        		map.put(s.charAt(i), t.charAt(i));
        	}
        }
        return true;
    }
    
    public static void main(String[] args){
//    	ListNode head=new ListNode(1);
//    	ListNode node2=new ListNode(2);
//    	ListNode node3=new ListNode(3);
//    	head.next=node2;
//    	node2.next=node3;
//    	
    	Code206 code=new Code206();
//    	head=code.reverseList(head);
//    	while(head!=null){
//    		System.out.println(head.val);
//    		head=head.next;
//    	}
    	code.isIsomorphic("ab", "aa");
    }
}
