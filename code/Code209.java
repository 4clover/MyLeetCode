package code;

public class Code209 {
    public int minSubArrayLen(int s, int[] nums) {
        int ans=Integer.MAX_VALUE;
        int n=nums.length;
        int l,r;
        int sum=0;
        for(r=0;r<n;r++){
        	sum+=nums[r];
        	if(sum>=s)
        		break;
        }
        if(r==n)
        	return 0;
        ans=r+1;
        l=0;
        for(;r<n;){
        	while(l<r && sum-nums[l]>=s){
        		sum-=nums[l];
        		l++;
        	}
        	if(sum>=s){
        		ans=ans>r-l+1?r-l+1:ans;
        	}
        	r++;
        	if(r<n)
        		sum+=nums[r];
        }
        return ans;
    }
    public static void main(String[] args){
    	int[] nums={1,2,3,4,5};
    	System.out.println(new Code209().minSubArrayLen(11, nums));
    }
}
