package code;

import java.util.Arrays;

public class Code215 {
    public int findKthLargest(int[] nums, int k) {

    	Arrays.sort(nums);
    	int n=nums.length;
    	return nums[n-k];
    }
    public static void main(String[] args){
    	int[] nums={3,2,1,5,6,4};
    	System.out.println(new Code215().findKthLargest(nums, 2));
    }
}
