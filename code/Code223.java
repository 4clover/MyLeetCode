package code;

public class Code223 {
	/*
	 * 思路Area(rect1)+Area(rect2)-Area(rect1*rect2)
	 * 减去公共部分
	 * dx*dy
	 * dx表示[x1,x2]与[x3,x4]的公共部分
	 * dy同理
	 */
    public int computeArea(int A, int B, int C, int D, int E, int F, int G, int H) {
    	int ans=(C-A)*(D-B)+(G-E)*(H-F);
    	int dx,dy;
    	/*
    	 * x没有交点
    	 */
    	if(A>=G || C<=E){
    		dx=0;
    	}
    	else{
    		if(A>=E){
    			if(C>=G)
    				dx=G-A;
    			else
    				dx=C-A;
    		}
    		else{
    			if(C<G)
    				dx=C-E;
    			else dx=G-E;
    		}
    	}
    	/*
    	 * y
    	 */
    	if(B>=H || D<=F){
    		dy=0;
    	}
    	else{
    		if(F>=B){
    			if(D>=H)
    				dy=H-F;
    			else dy=D-F;
    		}
    		else{
    			if(H>=D){
    				dy=D-B;
    			}
    			else	dy=H-B;
    		}
    	}
    	ans-=dx*dy;
        return ans;
    }
    public static void main(String[] args){
    	System.out.println(new Code223().computeArea(-3, 0, 3, 4, 0, -1, 9, 2));
    }
}
