package code;

import java.util.Stack;

public class Code224 {

	public int calculate(String s){
		int ans=0;
		int flag=1;
		s=s.replaceAll(" ", "");
		Stack<Integer> numSck=new Stack<Integer>();
		Stack<Character> opSck=new Stack<Character>();
		int i=0;
		int len=s.length();
		while(i<len || !opSck.isEmpty()){
			/*
			 * 字符串还没处理完
			 */
			if(i<len){
				if(s.charAt(i)=='+' || s.charAt(i)=='-'){
					opSck.push(s.charAt(i));
					i++;
				}
				else if(s.charAt(i)=='(' || s.charAt(i)==')'){
					/*
					 * '('入运算符栈
					 */
					if(s.charAt(i)=='('){
						opSck.push(s.charAt(i));
					}
					/*
					 * ')'，则弹出运算符'('
					 */
					else{
						opSck.pop();
					}
					i++;
					/*
					 * +-(x)
					 */
					while(!opSck.isEmpty() && opSck.peek()!='('){
						int a=numSck.pop();
						int b=numSck.pop();
						char op=opSck.pop();
						if(op=='+'){
							b+=a;
						}
						else{
							b-=a;
						}
						numSck.push(b);
					}
				}
				else {
					int x=0;
					while(i<len && s.charAt(i)>='0' && s.charAt(i)<='9'){
						x*=10;
						x+=(s.charAt(i)-'0');
						i++;
					}
					/*
					 * 如果当前的操作符栈不为空，则计算
					 */
					if(!opSck.isEmpty()){
						/*
						 * 如果操作栈运算符是'(',则将数直接压入 栈
						 */
						if(opSck.peek()=='('){
							numSck.push(x);
						}
						//a+-b运算，再压入栈
						else{
							int a=numSck.pop();
							char op=opSck.pop();
							if(op=='+'){
								a+=x;
							}
							else{
								a-=x;
							}
							/*
							 * 将计算好的数压入算数栈
							 */
							numSck.push(a);
						}
					}
					/*
					 * 否则将数压入算数栈
					 */
					else{
						numSck.push(x);
					}
				}
			}
			/*
			 * 整个字符串处理完，直接处理栈中的元素
			 */
			else{
				while(!opSck.isEmpty()){
					int a=numSck.pop();
					int b=numSck.pop();
					char op=opSck.pop();
					if(op=='+'){
						b+=a;
					}
					else b-=a;
					numSck.push(b);
				}
			}
		}
		if(numSck.isEmpty())
			return -1;
		return numSck.peek();
	}
	public static void main(String[] args){
		Code224 code=new Code224();
		System.out.println(code.calculate("  (7)-(0)+(4)"));
	}
}
