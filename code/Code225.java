package code;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

public class Code225 {
	
	private Queue<Integer> queue=new LinkedList<Integer>();
	private List<Integer> temp=new ArrayList<Integer>();
	
    // Push element x onto stack.
    public void push(int x) {
        queue.add(x);
    }

    // Removes the element on top of the stack.
    public void pop() {
        if(queue.isEmpty())
        	return ;
        while(!queue.isEmpty()){
        	temp.add(queue.poll());
        }
        temp.remove(temp.size()-1);
        for(int i=0;i<temp.size();i++){
        	queue.add(temp.get(i));
        }
        temp.clear();
    }

    // Get the top element.
    public int top() {
    	if(queue.isEmpty())
    		return 0;
        while(!queue.isEmpty()){
        	temp.add(queue.poll());
        }
        int ans=temp.get(temp.size()-1);
        for(int i=0;i<temp.size();i++){
        	queue.add(temp.get(i));
        }
        temp.clear();
        return ans;
    }

    // Return whether the stack is empty.
    public boolean empty() {
        return queue.isEmpty();
    }
}
