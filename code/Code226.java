package code;

public class Code226 {
	
	public TreeNode invertTree(TreeNode root){
		if(root==null)
			return null;
		TreeNode left=root.left;
		TreeNode right=root.right;
		if(left!=null)
			invertTree(left);
		if(right!=null)
			invertTree(right);
		root.left=right;
		root.right=left;
		return root;
	}
	
	public void dfs(TreeNode root){
		if(root==null)
			return ;
		System.out.println(root.val);
		dfs(root.left);
		dfs(root.right);
	}
	
	public static void main(String[] args){
		Code226 code=new Code226();
		TreeNode root=new TreeNode(4);
		TreeNode node7=new TreeNode(7);
		TreeNode node2=new TreeNode(2);
		TreeNode node1=new TreeNode(1);
		TreeNode node6=new TreeNode(6);
		TreeNode node9=new TreeNode(9);
		TreeNode node3=new TreeNode(3);
		root.left=node2;
		root.right=node7;
		node2.left=node1;
		node2.right=node3;
		node7.left=node6;
		node7.right=node9;
		
		code.invertTree(root);
		code.dfs(root);
	}
	
}
