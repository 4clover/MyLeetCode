package code;

public class Code24 {
    public ListNode swapPairs(ListNode head) {
    	ListNode pre,next;
    	ListNode p,q;
    	p=head;
    	pre=null;
    	while(p!=null){
    		q=p;
    		p=p.next;
    		if(p==null){
    			if(pre==null)
    				pre=head;
    			else
    				pre.next=q;
    			break;
    		}
    		next=p.next;
    		p.next=q;
    		if(pre==null){
    			head=p;
    			pre=q;
    		}
    		else{
    			pre.next=p;
    		}
    		pre=q;
    		p=next;
    		q.next=null;
    	}
        return head;
    }
    public static void main(String[] agrs){
    	ListNode head=new ListNode(1);
    	ListNode node2=new ListNode(2);
    	ListNode node3=new ListNode(3);
    	ListNode node4=new ListNode(4);
    	head.next=node2;
    	node2.next=node3;
//    	node3.next=node4;
    	
    	head=new Code24().swapPairs(head);
    	while(head!=null){
    		System.out.print(head.val+"-->");
    		head=head.next;
    	}
    	System.out.println();
    }
}
