package code;

public class Code26 {
	
    public int removeDuplicates(int[] nums) {
        int i,j;
        int cnt;
        int n=nums.length;
        cnt=0;
        for(i=0;i<n;){
        	nums[cnt++]=nums[i];
        	for(j=i+1;j<n && nums[i]==nums[j];j++)
        		;
        	if(j-i>1)
        		nums[cnt++]=nums[i];
        	i=j;
        }
        return cnt;
    }
    public static void main(String[] args){
    	Code26 code=new Code26();
    	int[] nums=new int[4];
    	nums[0]=1;
    	nums[1]=1;
    	nums[2]=2;
    	nums[3]=2;
    	code.removeDuplicates(nums);
    }
}
