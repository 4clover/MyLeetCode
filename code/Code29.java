package code;

public class Code29 {
	/*
	 * 注意在内存中-2147483648与0的表示方式一样，除了符号不一样，-2147483648表示-0
	 */
    public int divide(int dividend, int divisor) {
        long ans=0;
        int flag=0;
        if(divisor==0)
        	return Integer.MAX_VALUE;
        if(dividend>0){
        	flag=divisor>0?1:-1;
        }
        else{
        	flag=divisor>0?-1:1;
        }
        dividend=dividend>0?dividend:-dividend;
//        divisor=divisor>0?divisor:-divisor;
        long s=dividend;
        long t=divisor;
        t=t>0?t:-t;
        s=s>0?s:-s;
        long x=t;
        int cnt=1;
        while(true){
        	if(s<x)
        		break;
        	/*
        	 * 进行一轮减法
        	 * 但是减数按照*2的顺序进行
        	 * 因为每个数可以表示成2的多项式
        	 * x=an*2^n...+a0
        	 */
        	while(s>=x){
        		ans+=cnt;
        		s-=x;
        		x<<=1;
        		cnt<<=1;
        	}
        	x=t;
        	cnt=1;
        }
        ans*=flag;
        if(ans>Integer.MAX_VALUE)
        	return Integer.MAX_VALUE;
        return (int)ans;
    }
    public static void main(String[] args){
    	System.out.println(new Code29().divide(-2147483648, -1));
    }
}
