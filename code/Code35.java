package code;

public class Code35 {
	
	
    public int searchInsert(int[] nums, int target) {
        int ans=0;
        int l=0,r=nums.length-1,mid;
        if(r==-1)
        	return 0;
        while(l<=r){
        	mid=l+r>>1;
        	if(nums[mid]==target)
        		return mid;
        	if(nums[mid]<target){
        		l=mid+1;
        		ans=mid+1;
        	}
        	else r=mid-1;
        }
        
        return ans;
    }
    public static void main(String[] args){
    	int[] nums={1,3,5,6};
    	System.out.println(new Code35().searchInsert(nums, 0));
    }
}
