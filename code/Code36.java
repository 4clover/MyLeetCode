package code;

public class Code36 {
	
    public boolean isValidSudoku(char[][] board) {
        int i,j;
        
        for(i=0;i<9;i++)
        	for(j=0;j<9;j++){
        		boolean[] vis=new boolean[10];
        		int k;
        		//第i行
        		for(k=0;k<9;k++){
        			if(board[i][k]=='.')
        				continue;
        			if(vis[board[i][k]-'0']) 
        				return false;
        			vis[board[i][k]-'0']=true;
        		}
        		//第j列
        		for(k=0;k<10;k++)
        			vis[k]=false;
        		for(k=0;k<9;k++){
        			if(board[k][j]=='.' || i==k)
        				continue;
        			if(vis[board[k][j]-'0']) 
        				return false;
        			vis[board[k][j]-'0']=true;
        		}
        		/*
        		 * 以x=i/3*3,y=i/3*3为起点的3*3的格子
        		 */
        		for(k=0;k<10;k++)
        			vis[k]=false;
        		int x=i/3*3,y=j/3*3;
        		int l;
        		for(k=x;k<x+3;k++)
        			for(l=y;l<y+3;l++){
        				if(board[k][l]=='.' || k==i && l==j)
        					continue;
        				if(vis[board[k][l]-'0']) 
            				return false;
        				vis[board[k][l]-'0']=true;
        			}
        	}
        return true;
    }
    
    public static void main(String[] args){
    	char[][] board={{'.','.','.','.','.','7','.','.','9'},
    			{'.','4','.','.','8','1','2','.','.'},
    			{'.','.','.','9','.','.','.','1','.'},
    			{'.','.','5','3','.','.','.','7','2'},
    			{'2','9','3','.','.','.','.','5','.'},
    			{'.','.','.','.','.','5','3','.','.'},
    			{'8','.','.','.','2','3','.','.','.'},
    			{'7','.','.','.','5','.','.','4','.'},
    			{'5','3','1','.','7','.','.','.','.'}};
    	System.out.println(new Code36().isValidSudoku(board));
    }
}
