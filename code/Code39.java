package code;

import java.util.ArrayList;
import java.util.List;

public class Code39 {
	
    public String countAndSay(int n) {
    	StringBuilder x=new StringBuilder("1");
    	n--;
    	while(n-->0){
    		StringBuilder sb=new StringBuilder();
	        int i,j;
	        for(i=0;i<x.length();){
	        	for(j=i+1;j<x.length() && x.charAt(i)==x.charAt(j);j++)
	        		;
	        	sb.append(String.valueOf(j-i)+x.charAt(i));
	        	i=j;
	        }
	        x=sb;
    	}
        return x.toString();
    }
    public static void main(String[] args){
    	System.out.println(new Code39().countAndSay(3));
    }
}
