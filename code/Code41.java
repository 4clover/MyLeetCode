package code;

import java.util.HashSet;
import java.util.Set;

public class Code41 {
    public int firstMissingPositive(int[] nums) {
    	int i,j;
    	Set<Integer> has=new HashSet<Integer>();
    	int n=nums.length;
    	for(i=0;i<n;i++){
    		has.add(nums[i]);
    	}
    	int min=1;
    	while(has.contains(min)){
    		min++;
    	}
        return min;
    }
    public static void main(String[] args){
    	int[] A={2};
    	System.out.println(new Code41().firstMissingPositive(A));
    }
}
