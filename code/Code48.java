package code;

public class Code48 {
    public void rotate(int[][] matrix) {
        int n=matrix.length;
        if(n==0)
        	return;
        int[][] rMatrix=new int[n][n];
        int i,j;
        for(i=0;i<n;i++)
        	for(j=0;j<n;j++){
        		rMatrix[i][j]=matrix[n-j-1][i];
        	}
        for(i=0;i<n;i++)
        	for(j=0;j<n;j++){
        		matrix[i][j]=rMatrix[i][j];
        	}
    }
    public static void main(String[] args){
    	int[][] matrix={{1,2,3},{4,5,6},{7,8,9}};
    	new Code48().rotate(matrix);
    }
}
