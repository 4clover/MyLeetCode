package code;

public class Code59 {
	
    public int[][] generateMatrix(int n) {
        int[][] matrix=new int[n][n];
        int i,j;
        int[][] direction={{0,1},{1,0},{0,-1},{-1,0}};
        int startX=0,startY=-1;
        int cnt;
        for(i=0;i<n;i++)
        	for(j=0;j<n;j++)
        		matrix[i][j]=-1;
        cnt=1;
        while(cnt<=n*n){
        	/*
        	 * 绕一圈
        	 */
        	for(i=0;i<4;i++){
        		/*
        		 * 每次都走完之后，改变方向
        		 */
        		startX+=direction[i][0];
        		startY+=direction[i][1];
        		while(isLegal(startX, startY, n) && matrix[startX][startY]==-1){
        			matrix[startX][startY]=cnt++;
	        		startX+=direction[i][0];
	        		startY+=direction[i][1];
        		}
        		//回退一步
        		startX-=direction[i][0];
        		startY-=direction[i][1];
        	}
        }
        return matrix;
    }
    
    public boolean isLegal(int x,int y,int n){
    	if(x>=0 && x<n && y>=0 && y<n)
    		return true;
    	return false;
    }
    public int lengthOfLastWord(String s) {
        String[] A=s.split(" ");
        if(A.length==0)
        	return 0;
        
        return A[A.length-1].length();
    }
    public static void main(String[] args){
//    	new Code59().generateMatrix(3);
    	System.out.println(new Code59().lengthOfLastWord("hello world"));
    }
}
