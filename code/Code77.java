package code;

import java.util.ArrayList;
import java.util.List;

public class Code77 {
	
	/*
	 * 求Cn,k的组合
	 */
	public void dfs(boolean[] vis,int step,int p,int k,int n,List<List<Integer>> result){
		int i;
		if(step==k){
			List<Integer> list=new ArrayList<Integer>();
			for(i=1;i<=n;i++){
				if(vis[i])
					list.add(i);
			}
			result.add(list);
			return;
		}
		for(i=p+1;i<=n;i++){
			if(!vis[i]){
				vis[i]=true;
				dfs(vis,step+1,i,k,n,result);
				vis[i]=false;
			}
		}
	}
    public List<List<Integer>> combine(int n, int k) {
        List<List<Integer>> result=new ArrayList<List<Integer>>();
        boolean[] vis=new boolean[n+1];
        dfs(vis,0,0,k,n,result);
        return result;
    }
    
    public static void main(String[] args){
    	Code77 code=new Code77();
    	code.combine(10, 2);
    }
}
