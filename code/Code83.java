package code;

public class Code83 {
	
    public ListNode deleteDuplicates(ListNode head) {
    	ListNode p,q;
    	p=head;
    	while(p!=null){
    		int val=p.val;
    		q=p.next;
    		while(q!=null && q.val==val){
    			q=q.next;
    		}
    		p.next=q;
    		p=q;
    	}
        return head;
    }
    public static void main(String[] args){
    	ListNode head=new ListNode(1);
    	ListNode x=new ListNode(1);
    	ListNode y=new ListNode(1);
    	ListNode z=new ListNode(2);
    	head.next=x;
    	x.next=y;
    	y.next=z;
    	Code83 code=new Code83();
    	head=code.deleteDuplicates(head);
    	x=head;
    	while(x!=null){
    		System.out.println(x.val);
    		x=x.next;
    	}
    }
}
