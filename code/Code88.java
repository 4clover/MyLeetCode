package code;

public class Code88 {
	
    public void merge(int[] nums1, int m, int[] nums2, int n) {
        int[] ans=new int[n+m];
        int i=0,j=0;
        int cnt=0;
        while(i<m && j<n){
        	while(i<m && nums1[i]<=nums2[j]){
        		ans[cnt++]=nums1[i];
        		i++;
        	}
        	if(i==m)	
        		break;
        	while(j<n && nums1[i]>nums2[j]){
        		ans[cnt++]=nums2[j];
        		j++;
        	}
        }
        if(i<m){
        	while(i<m){
        		ans[cnt++]=nums1[i];
        		i++;
        	}
        }
        if(j<n){
        	while(j<n){
        		ans[cnt++]=nums2[j];
        		j++;
        	}
        }
        for(i=0;i<cnt;i++){
        	nums1[i]=ans[i];
//        	System.out.print(ans[i]+" ");
        }
//        System.out.println();
    }
    
    public static void main(String[] args){
    	Code88 code=new Code88();
    	int[] nums1=new int[6];
    	nums1[0]=1;
    	nums1[1]=3;
    	nums1[2]=5;
    	int[] nums2={2,4,6};
    	code.merge(nums1, 3, nums2, 3);
    }
}
