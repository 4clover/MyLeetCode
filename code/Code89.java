package code;

import java.util.ArrayList;
import java.util.List;

public class Code89 {
	
    public List<Integer> grayCode(int n) {
        int i;
        List<Integer> list=new ArrayList<Integer>();
        int size=1<<n;
        for(i=0;i<size;i++){
        	int x=i ^ i>>1;
        	list.add(x);
        }
        return list;
    }
    
	public static void main(String[] args){
		Code89 code=new Code89();
		code.grayCode(3);
	}
}
