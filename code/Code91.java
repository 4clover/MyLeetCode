package code;

public class Code91 {
	
    public int numDecodings(String s) {
    	int i;
    	int n=s.length();
    	int[] dp=new int[n+1];
    	if(n==0)
    		return 0;
    	dp[0]=1;
    	for(i=1;i<=s.length();i++){
    		if(s.charAt(i-1)>'0')
    			dp[i]=dp[i-1];
    		if(i>1){
	    		int x=Integer.parseInt(s.substring(i-2,i));
	    		if(x>9 && x<=26)
	    			dp[i]+=dp[i-2];
    		}
    	}
        return dp[n];
    }
    public static void main(String[] args){
    	System.out.println(new Code91().numDecodings("1234"));
    }
}
