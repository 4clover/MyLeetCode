package code;

import java.util.ArrayList;
import java.util.List;

public class Code94 {

	/*
	 * bst 中序遍历
	 */
	public void bstInorderTraversal(TreeNode p,List<Integer> list){
		if(p==null)
			return;
		if(p.left!=null)
			bstInorderTraversal(p.left, list);
		list.add(p.val);
		if(p.right!=null)
			bstInorderTraversal(p.right, list);
	}
    public List<Integer> inorderTraversal(TreeNode root) {
        List<Integer> result=new ArrayList<Integer>();
        bstInorderTraversal(root, result);
        return result;
    }
}
