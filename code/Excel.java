package code;

import java.util.HashMap;
import java.util.Map;
import java.util.Stack;


public class Excel {
	public String convertToTitle(int n) {
    	StringBuilder sb=new StringBuilder();
    	Map<Integer,Character> map=new HashMap<Integer,Character>();
    	for(int i=0;i<26;i++){
    		map.put(i, (char) ('A'+i));
    	}
    	Stack<Integer> stack=new Stack<Integer>();
    	while(true){
    		n--;
    		stack.push(n%26);
    		n/=26;
    		if(n==0)	break;
    	}
    	while(!stack.empty()){
    		int x=stack.peek();
    		sb.append(map.get(x));
    		System.out.println(stack.peek()+" "+map.get(x));
    		stack.pop();
    	}
        return sb.toString();
    }
	public static void main(String[] args){
		System.out.println((int)(12/13));
		Excel excel=new Excel();
		System.out.println(excel.convertToTitle(26));
	}
}
