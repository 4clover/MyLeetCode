package code;


public class FastPow {
	
	public double cal_pow(double x, int n){
		double ans=1;
        if(n==1)	return x;
        if(n==0)	return 1;
        if((n&1)==1) {
        	ans*=x;
        }
        double y=pow(x,n/2);
        return ans*y*y;
	}
    public double pow(double x, int n) {
    	boolean flag=true;
    	if(n<0)	{
    		n=-n;
    		flag=false;
    	}
    	double ans=cal_pow(x,n);
    	if(!flag)	ans=1/ans;
        return ans;
    }
    
	public static void main(String[] args){
		FastPow fp=new FastPow();
		System.out.println(fp.pow(34.00515, -3));
//		for(int i=0;i<30;i++)
//			System.out.println(fp.pow(2, -i));
	}
}
