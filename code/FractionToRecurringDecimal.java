package code;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;


public class FractionToRecurringDecimal {
	
	public String fractionToDecimal(int numerator, int denominator) {
		Map<Long,Integer> map=new HashMap<Long,Integer>();
		String res="";
		long num=numerator;
		long den=denominator;
		if(num*den<0){
			num=Math.abs(num);
			den=Math.abs(den);
			res+="-";
		}
		res+=String.valueOf(num/den);
		num%=den;
		if(num==0)	return res;
		res+=".";
		num*=10;
		ArrayList<Integer> list=new ArrayList<Integer>();
		int loopStartIndex=-1;
		int i;
		while(num!=0){
			if(map.containsKey(num)){
				loopStartIndex=map.get(num);
				String part1=res.substring(0,loopStartIndex);
				String part2=res.substring(loopStartIndex,res.length());
				res=part1+"("+part2+")";
				break;
			}
			map.put(num, res.length());
			res+=String.valueOf(num/den);
			num%=den;
			num*=10;
		}
		return res;
    }
	
	public static void main(String[] args){
		FractionToRecurringDecimal ftrd=new FractionToRecurringDecimal();
//		System.out.println(ftrd.fractionToDecimal(13, 14));
//		System.out.println(ftrd.fractionToDecimal(2, 3));
		System.out.println(ftrd.fractionToDecimal(1, 90));
	}
}
