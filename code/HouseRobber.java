package code;

import java.util.ArrayList;
import java.util.List;

public class HouseRobber {
	public int rob(int[] num){
		int n=num.length;
		if(n==0)	return 0;
		int[] dp=new int[n];
		dp[0]=num[0];
		for(int i=1;i<n;i++){
			if(i==1){
				if(num[i]>num[i-1])	dp[i]=num[i];
				else dp[i]=dp[i-1];
			}
			else{
				if(dp[i-1]<dp[i-2]+num[i])	dp[i]=dp[i-2]+num[i];
				else dp[i]=dp[i-1];
			}
		}
		return dp[n-1];
	}
    public boolean isPalindrome(String s) {
    	int n=s.length();
    	if(n==0)	return true;
    	int l=0,r=n-1;
    	while(l<r){
    		while(l<r && !isOk(s.charAt(l))) l++;
    		if(l==r)	break;
    		while(l<r && !isOk(s.charAt(r))) r--;
    		
    		System.out.println(s.charAt(l)+"  "+s.charAt(r));
    		int differce=Math.abs((int)(s.charAt(l)-s.charAt(r)));
    		if(differce==0 || differce==32){
	    		l++;
	    		r--;
    		}
    		else return false;
    	}
        return true;
    }
    public boolean isOk(char c){
    	return c>='A' && c<='Z' || c>='a' && c<='z' || c>='0' && c<='9';
    }
    
    public List<Integer> getRow(int rowIndex) {
    	List<Integer> result=new ArrayList<Integer>();
    	
        return result;
    }
	public static void main(String[] args){
		int[] num={1,1,1};
		String s="A man, a plan, a canal: Panama";
		HouseRobber hr=new HouseRobber();
		System.out.println((int)('a'-'A'));
		System.out.println(hr.isPalindrome(s));
	}
}
