package code;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;

import org.omg.CORBA.INTERNAL;


public class LargestNumber {
	
	public String largestNumber(int[] num) {
        String sb="";
        
        String[] list=new String[num.length];
        int i,j;
        for(i=0;i<num.length;i++){
        	list[0]=String.valueOf(num[i]);
        }
        Arrays.sort(list,new Cmp());
        for(i=0;i<num.length;i++){
        	sb+=(list[i]);
        }
        for(i=0;i<sb.length() &&sb.charAt(i)=='0' ;i++);
        sb=sb.substring(i, sb.length());
        
        return sb.toString();
    }
	
	class Cmp implements Comparator<String>{

		@Override
		public int compare(String a,String b) {
			// TODO Auto-generated method stub
			String ab=a+b;
			String ba=b+a;
			return Integer.parseInt(ab)-Integer.parseInt(ba);
		}
	}
	
	public static void main(String[] args){
		int[] a={3, 30, 300};
		LargestNumber ln=new LargestNumber();
		System.out.println(ln.largestNumber(a));
	}
}
