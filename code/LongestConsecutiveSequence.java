package code;

import java.util.HashMap;
import java.util.Map;

public class LongestConsecutiveSequence {

	/*
	 * ������Ĵ��룬��Ľ�
	 */
	
    public int longestConsecutive(int[] num) {
    	int n=num.length;
    	Map<Integer,Integer> hash=new HashMap<Integer,Integer>();
    	int i,j;
    	for(i=0;i<n;i++)
    		hash.put(num[i], 1);
    	int ans=0;
    	for(i=0;i<n;i++){
    		for(j=num[i]+1;j<n;j++){
    			if(!hash.containsKey(j))
    				break;
    			hash.remove(j);
    		}
    		hash.put(num[i], j-num[i]);
    		if(ans<j-num[i])
    			ans=j-num[i];
    	}
        return ans;
    }
    public static void main(String[] args){
    	int[] A={-1,0,1};
    	LongestConsecutiveSequence lcs=new LongestConsecutiveSequence();
    	System.out.println(lcs.longestConsecutive(A));
    }
}
