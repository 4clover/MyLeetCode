package code;

import java.util.Scanner;

public class Main{
	
	static public class Point{
		int x;
		int y;
		Point(int a,int b){
			x=a;
			y=b;
		}
		Point(Point a){
			x=a.x;
			y=a.y;
		}
	}
	public static void main(String[] args){
		int n,m;
		int S;
		Scanner scan=new Scanner(System.in);
		m=scan.nextInt();
		n=scan.nextInt();
		S=scan.nextInt();
		char[][] map=new char[n][m];
		
		Point player = null,box=null,target = null;
		
		for(int i=0;i<n;i++){
			String row=scan.next();
//			System.out.println("-----> "+row+" "+row.length());
			for(int j=0;j<row.length();j++){
				map[i][j]=row.charAt(j);
				if(map[i][j]=='1'){
					player=new Point(i,j);
				}
				else if(map[i][j]=='3'){
					box=new Point(i,j);
				}
				else if(map[i][j]=='2'){
					target=new Point(i,j);
				}
			}
		}
		System.out.println("箱子的位置 "+box.x+" "+box.y);
		System.out.println("玩家的位置 "+player.x+" "+player.y);
		System.out.println("目标位置 "+target.x+" "+target.y);
		for(int cases=0;cases<S;cases++){
			int T;
			T=scan.nextInt();
			String op=scan.next();
//			System.out.println(T+" "+op);
			Point player1,box1;
			player1=new Point(player);
			box1=new Point(box);
			boolean flag=false;
			for(int i=0;i<op.length();i++){
				System.out.println(i+" "+op.charAt(i));
				//判断箱子是不是已经在目标点了
				if(box1.x==target.x && box1.y==target.y)
				{
					System.out.println("YES");
					flag=true;
					break;
				}
				//判断箱子是否在角落
				if(box1.x==0 && (box1.y==0 || box1.y==m-1))
				{
					System.out.println("NO");
					break;
				}
				if(box1.x==n-1 && (box1.y==0 || box1.y==m-1)){
					System.out.println("NO");
					break;
				}
				char d=op.charAt(i);
				//u向上
				if(d=='u'){
					//玩家碰到墙了,玩家是可以动的，箱子碰到了墙就不能移动
					if(player1.x==0){
						//...0的墙，玩家不移动
					}
					else{
						//判断箱子是不是在玩家的正上方
						if(box1.x==player1.x-1 && box1.y==player1.y){
							//箱子没遇到墙,要到的点可以走
							if(box1.x>0 && map[box1.x-1][box1.y]!='4'){
								box1.x--;
								player1.x--;
							}
						}
						//箱子不在玩家上方，移动玩家位置就够了
						else{
							if(player1.x>0 && map[player1.x-1][player1.y]!='4')
								player1.x--;
						}
					}
				}//d向下
				else if(d=='d'){
					//玩家碰到下墙
					if(player1.x==n-1){
						//不操作
					}
					else{
						//box在player正下方
						if(box1.x==player1.x+1 && box1.y==player1.y){
							//box没遇墙
							if(box1.x<n-1 && map[box1.x+1][box1.y]!='4')
							{
								box1.x++;
								player1.x++;
							}
						}
						else{//直接移动玩家
							if(player1.x<n-1 && map[player1.x+1][player1.y]!='4'){
								player1.x++;
							}
						}
					}
				}//l向左
				else if(d=='l'){
					//遇墙
					if(player1.y==0){
						//不操作
					}
					else{
						//box在玩家的左边
						if(box1.y==player1.y-1 && box1.x==player1.x){
							if(box1.y>0 && map[box1.x][box1.y-1]!='4'){
								box1.y--;
								player1.y--;
							}
						}
						//直接移动玩家
						else{
							if(player1.y>0 && map[player1.x][player1.y-1]!='4'){
								player1.y--;
							}
						}
					}
				}
				//r
				else if(d=='r'){
					//玩家撞墙了
					if(player1.y==m-1){
						
					}
					else{
						//box 在人的→_→
						if(box1.y==player1.y+1 && box1.x==player1.x){
							if(box1.y<m-1 && map[box1.x][box1.y+1]!='4'){
								box1.y++;
								player1.y++;
							}
						}//直接移动玩家
						else{
							if(player1.y<m-1 && map[player1.x][player1.y+1]!='4'){
								player1.y++;
							}
						}
					}
				}
				System.out.println("箱子的位置 "+box1.x+" "+box1.y);
				System.out.println("玩家的位置 "+player1.x+" "+player1.y);
				//再判断箱子是不是已经在目标点了
				if(box1.x==target.x && box1.y==target.y)
				{
					System.out.println("YES");
					flag=true;
					break;
				}
			}
			if(!flag){
				System.out.println("NO");
			}
		}
	}
}
/*

5 4 3
00000
13000
00200
00000
4 rurd
6 urdldr
6 rrrurd
 */