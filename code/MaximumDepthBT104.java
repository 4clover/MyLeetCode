package code;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;
import java.util.Queue;


public class MaximumDepthBT104 {
    public int minDepth(TreeNode root) {
    	if(root==null)
    		return 0;
    	Queue<TreeNode> queue=new LinkedList<TreeNode>();
    	Map<TreeNode,Integer> map=new HashMap<TreeNode,Integer>();
    	queue.add(root);
    	map.put(root, 1);
    	TreeNode head=null;
    	while(!queue.isEmpty()){
    		head=queue.poll();
    		int dis=map.get(head);
    		if(head.left!=null){
    			queue.add(head.left);
    			map.put(head.left, dis+1);
    		}
    		if(head.right!=null){
    			queue.add(head.right);
    			map.put(head.right, dis+1);
    		}
    	}
        return map.get(head);
    }
}
