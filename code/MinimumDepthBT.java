package code;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;
import java.util.Queue;


public class MinimumDepthBT {
	
    public int minDepth(TreeNode root) {
    	if(root==null)
    		return 0;
    	Queue<TreeNode> queue=new LinkedList<TreeNode>();
    	Map<TreeNode,Integer> map=new HashMap<TreeNode,Integer>();
    	queue.add(root);
    	map.put(root, 1);
    	while(!queue.isEmpty()){
    		TreeNode head=queue.poll();
    		int dis=map.get(head);
    		if(head.left==null && head.right==null)
    			return dis;
    		
    		if(head.left!=null){
    			queue.add(head.left);
    			map.put(head.left, dis+1);
    		}
    		if(head.right!=null){
    			queue.add(head.right);
    			map.put(head.right, dis+1);
    		}
    	}
        return 0;
    }
    public static void main(String[] args){
    	
    }
}
