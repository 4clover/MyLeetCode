
package code;

import java.util.ArrayList;
import java.util.List;

public class Pascal {
    public List<Integer> getRow(int rowIndex) {
    	
    	rowIndex++;
    	int[][] dp=new int[2][rowIndex+1];
    	int i,j,k;
    	dp[0][0]=1;
    	k=0;
    	for(i=1;i<rowIndex;i++){
    		for(j=0;j<=i;j++){
    			if(j==0){
    				dp[(k+1)%2][j]=dp[k%2][j];
    			}
    			else if(j==i){
    				dp[(k+1)%2][j]=1;
    			}
    			else{
    				dp[(k+1)%2][j]=dp[k%2][j-1]+dp[k%2][j];
    			}
    		}
//    		for(j=0;j<=i;j++){
//    			System.out.print(dp[(k+1)%2][j]+" ");
//    		}
//    		System.out.println();
    		k++;
    	}
    	List<Integer> result=new ArrayList<Integer>();
    	for(i=0;i<rowIndex;i++){
    		result.add(dp[k%2][i]);
    	}
//    	for(i=0;i<rowIndex;i++){
//    		System.out.println(result.get(i));
//    	}
        return result;
    }
    public static void main(String[] args){
    	new Pascal().getRow(1);
    }
}
