package code;


public class RotateArray {
	public void rotate(int[] nums, int k) {
        int i,j;
        int N=nums.length;
        int[] tmp=new int[N];
        int cnt=0;
        for(i=N-k;i<N;i++){
            tmp[cnt++]=nums[i];
        }
        for(i=0;i<N-k;i++){
            tmp[cnt++]=nums[i];
        }
        for(i=0;i<N;i++)
            nums[i]=tmp[i];
        for(i=0;i<N;i++){
        	System.out.println(nums[i]);
        }
    }
	public static void main(String[] args){
		RotateArray ra=new RotateArray();
		int[] nums={1,2,3,4,5,6,7};
		
		ra.rotate(nums, 3);
	}
}
