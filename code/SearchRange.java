package code;

public class SearchRange {

	/*
	 * ���ֵ���Сλ��
	 */
	public int BinarySearchMin(int[] A,int x){
		int l,r,mid;
		l=0;
		r=A.length-1;
		int ans=-1;
		while(l<r){
			mid=l+r>>1;
			if(A[mid]>=x){
				ans=mid;
				r=mid-1;
			}
			else l=mid+1;
		}
		if(A[l]==x)	ans=l;
		if(ans!=-1 && A[ans]==x)	return ans;
		return -1;
	}
	
	/*
	 * ���ֵ����λ��
	 */
	public int BinarySearchMax(int[] A,int x){
		int l,r,mid;
		l=0;
		r=A.length-1;
		int ans=-1;
		while(l<r){
			mid=l+r>>1;
			if(A[mid]<=x){
				ans=mid;
				l=mid+1;
			}
			else r=mid-1;
		}
		if(A[l]==x && ans<l)	ans=l;
		if(ans!=-1 && A[ans]==x)	return ans;
		return -1;
	}
	
    public int[] searchRange(int[] A, int target) {
        int start=BinarySearchMin(A, target);
        int end=BinarySearchMax(A, target);
        System.out.println(start+" "+end);
        int[] ans={start,end};
        return ans;
    }
	public static void main(String[] ags){
		int[] A={1,2,3};
		SearchRange sr=new SearchRange();
		System.out.println(sr.searchRange(A, 1));
	}
}
