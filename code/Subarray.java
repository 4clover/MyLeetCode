package code;


public class Subarray {

    public int maxSubArray(int[] A) {
        int maxSum=0;
        int n=A.length;
        int ans=Integer.MIN_VALUE;
        for(int i=0;i<n;i++){
        	maxSum+=A[i];
        	if(ans<maxSum){
        		ans=maxSum;
        	}
        	if(maxSum<0){
        		maxSum=0;
        	}
        }
        return ans;
    }
    public static void main(String[] args){
    	int[] a={-2,1,-3,4,-1,2,1,-5,4};
    	Subarray sa=new Subarray();
    	System.out.println(sa.maxSubArray(a));
    }
}
