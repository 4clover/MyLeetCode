package code;

import java.util.HashMap;
import java.util.Map;
import java.util.Stack;

public class Trailing {
	public int trailingZeroes(int n) {
		int ans=0;
		while(n>1){
			ans+=n/5;
			n/=5;
		}
		return ans;
    }
	public int titleToNumber(String s) {
		int ans=0;
		for(int i=0;i<s.length();i++){
			ans*=26;
			ans+=(s.charAt(i)-'A')+1;
		}
        return ans;
    }
	public int majorityElement(int[] num){
		int ans=0;
		HashMap<Integer,Integer> map=new HashMap<Integer,Integer>();
		int size=(num.length+1)/2;
		
		for(int i=0;i<num.length;i++){
			if(map.containsKey(num[i])){
				map.put(num[i], map.get(num[i])+1);
				if(map.get(num[i])>=size)
				{
					ans=num[i];
					break;
				}
			}
			else{
				map.put(num[i], 1);
				if(map.get(num[i])>=size)
				{
					ans=num[i];
					break;
				}
			}
		}
		return ans;
	}
    public String convertToTitle(int n) {
    	StringBuilder sb=new StringBuilder();
    	Map<Integer,Character> map=new HashMap<Integer,Character>();
    	for(int i=0;i<26;i++){
    		map.put(i, (char) ('A'+i));
    	}
    	Stack<Integer> stack=new Stack<Integer>();
    	n-=1;
    	while(true){
    		stack.push(n%26);
    		n/=26;
    		if(n==0)	break;
    	}
    	while(!stack.empty()){
    		sb.append(map.get(stack.peek()));
    		System.out.println(stack.peek()+" "+map.get(stack.peek()));
    		stack.pop();
    	}
        return sb.toString();
    }
	public static void main(String[] args){
		Trailing tl=new Trailing();
//		System.out.println(tl.trailingZeroes(25));
//		System.out.println(tl.titleToNumber("AB"));
		System.out.println(tl.convertToTitle(27));
	}
}
