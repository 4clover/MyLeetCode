package code;


public class UnquePaths {
    public int uniquePaths(int m, int n) {
    	int[][] dp=new int[m][n];
    	dp[0][0]=1;
    	for(int i=0;i<m;i++)
    	{
    		for(int j=0;j<n;j++){
    			if(i==0 &&	j==0)	continue;
    			if(i==0){
    				dp[i][j]=dp[i][j-1];
    			}
    			if(j==0){
    				dp[i][j]=dp[i-1][j];
    			}
    			if(i>0 &&j>0){
    				dp[i][j]=dp[i-1][j]+dp[i][j-1];
    			}
    		}
    	}
        return dp[m-1][n-1];
    }
    public static void main(String[] args){
    	UnquePaths up=new UnquePaths();
    	System.out.println(up.uniquePaths(50, 100));
    }
}
