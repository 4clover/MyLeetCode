package wap;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

/*
 * exam2
 * wap traveral center
 */
public class Main {
	/*
	 * dis[i],表示离最近festival city的距离
	 * 查询O(1),总共更新的次数一共O(n*n)
	 */
	
	public void solved(){
		int n,m;
		List<List<Integer>> edge;
		int i,j;
		Scanner input=new Scanner(System.in);
		n=input.nextInt();
		m=input.nextInt();
		edge=new ArrayList<List<Integer>>();
		for(i=0;i<n;i++){
			edge.add(new ArrayList<Integer>());
		}
		
		int[] dis=new int[n];
		/*
		 * add edges
		 */
		for(i=0;i<n-1;i++){
			int a,b;
			a=input.nextInt();
			b=input.nextInt();
			a--;
			b--;
			edge.get(a).add(b);
			edge.get(b).add(a);
		}
		//initialize dis[] be big enough for once city(0) to update all city nodes
		
		for(i=0;i<n;i++){
			dis[i]=n+1;
		}
		/*
		 * Tag the visited city during tree traverse
		 */
		boolean[] vis=new boolean[n];
		update(0,0,dis,edge,vis);
		/*
		 * handle query & update dis[i]
		 */
		for(int ncase=0;ncase<m;ncase++){
			int tag=input.nextInt();
			int c=input.nextInt();
			c--;
			if(tag==1){
				// mark city c be a festival city & update dis[i...]
				update(c,0,dis,edge,vis);
			}
			else{//query city c‘s shortest festival city distance
				System.out.println(dis[c]);
			}
		}
	}
	/*
	 * update dis[],from new festival city p,and step is the distance from this new festival
	 */
	
	public void update(int p,int step,int[] dis,List<List<Integer>> edge,boolean[] vis){
		/*
		 * when　dis[p]<step, no need to update p's subTree
		 */
		if(dis[p]<step)
			return ;
		vis[p]=true;
		dis[p]=step;
		for(int i=0;i<edge.get(p).size();i++){
			int u=edge.get(p).get(i);
			// u has not been visited
			if(!vis[u]){
				update(u,step+1,dis,edge,vis);
			}
		}
		vis[p]=false;
	}
	
	public static void main(String[] args){
		Main mm=new Main();
		mm.solved();
	}
}
/*

5 5
1 2
1 3
3 4
3 5
2 5
2 3
1 5
2 3
2 4
 */
